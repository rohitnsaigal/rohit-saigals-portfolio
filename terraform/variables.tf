variable "site_domain"{
    type = string
    description = "simple website domain"
    default = "portfolio.rohitsaigal.com"

}

variable "route53_domain"{
    type = string
    description = "simple website domain"
    default = "portfolio.rohitsaigal.com"

}

variable "route53_zone_id"{
    type = string
    description = "zone id for rohitsaigal.com"
    default= "Z0645753DPQBRL70UE1L"
}