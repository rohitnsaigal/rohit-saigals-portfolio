# DNS validation to create a hosted zone
resource "aws_acm_certificate" "cert" {
  domain_name       = "${var.site_domain}"
  subject_alternative_names=["www.${var.site_domain}"]
  validation_method = "DNS"
}

# THE FOLLOWING IS ONLY NEEDED IF CREAING A NEW HOSTED ZONE
# data "aws_route53_zone" "zone" {
#   name = "${var.route53_domain}"
#   private_zone=false
# }


resource "aws_route53_record" "cert_validation" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.route53_zone_id
}

resource "aws_acm_certificate_validation" "cert" {
  certificate_arn         = "${aws_acm_certificate.cert.arn}"
  validation_record_fqdns = [for record in aws_route53_record.cert_validation : record.fqdn]
}

# route 53 redirect record 
resource "aws_route53_record" "www" {
  zone_id = "${var.route53_zone_id}"
  name    = "${var.site_domain}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.site.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.site.hosted_zone_id}"
    evaluate_target_health = false
  }
} 

# route 53 redirect record 
resource "aws_route53_record" "redirect-wwww" {
  zone_id = "${var.route53_zone_id}"
  name    = "www.${var.site_domain}"
  type    = "A"

  alias {
    name                   = "${aws_cloudfront_distribution.site.domain_name}"
    zone_id                = "${aws_cloudfront_distribution.site.hosted_zone_id}"
    evaluate_target_health = false
  }
} 