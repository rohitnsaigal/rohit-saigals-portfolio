resource "aws_s3_bucket" "site" {
    bucket = var.site_domain
    acl = "public-read"
    force_destroy = true

    website {
        index_document = "index.html"
        error_document = "index.html"
    }
}

resource "aws_s3_bucket_policy" "public_read" {
    bucket = var.site_domain
    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            {
                Sid = "AllowReadFromAll"
                Effect = "Allow"
                Principal = "*"
                Action = "s3:GetObject"
                Resource = [
                    aws_s3_bucket.site.arn,
                     "${aws_s3_bucket.site.arn}/*",
                ]
            },
        ]
    })
}

# upload of the s3 objects

resource "null_resource" "remove_and_upload_to_s3" {
  provisioner "local-exec" {
    command = "aws s3 sync ${join("/",slice(split("/",abspath(path.module)),0, length(split("/",abspath(path.module))) - 1 ))}/build s3://${aws_s3_bucket.site.id}"
  }
}
